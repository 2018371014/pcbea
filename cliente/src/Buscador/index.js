import React from "react";
import './buscador.css';


function Buscador({buscarValor,setBuscarValor}){
    const buscar = (event) =>{
        setBuscarValor(event.target.value);
    }
    return( 
    <div class="searchBox">
    <input
    value={buscarValor}
    onChange={buscar} 
    class="searchInput"
    type="text" name="" placeholder="Buscar Nota..."/>
    <button class="searchButton" href="#">
    <span role="img" aria-label="Boton buscar">🔎
    </span>
    </button>
</div>
       

   

 )
}
export {Buscador}