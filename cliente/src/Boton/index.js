import React from "react";
import './boton.css';
import Modal from 'react-modal';
import axios from "axios";

const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };

function Boton(props){
    let subtitle;
    const [modalIsOpen, setIsOpen] = React.useState(false);
  
    function openModal() {
      setIsOpen(true);
    }
  
    function afterOpenModal() {
      // references are now sync'd and can be accessed.
      subtitle.style.color = '#06296b';
    }
  
    function closeModal() {
      setIsOpen(false);
    }
  
    const [tarea, settarea] = React.useState('');
  
    const onChange = (event) => {
      settarea(event.target.value);
    };
  
    const onSubmit = (event) => {
      event.preventDefault();
      props.addTarea(tarea);
      setIsOpen(false);
      axios.post('api',{
        tarea
      }).then(res=>
        console.log('peticion post',res)
        ).catch(err => console.log(err)  )
    };
    /*function OnClickButton(){
        alert('Abriendo modal de agregar tarea.')
    }*/
    return(
        <div>
            <button
            class="btn-hover color-7" 
            onClick={openModal}
            >
            AGREGAR TAREA +
            </button>
        <Modal 
        isOpen={modalIsOpen}
        onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Modal"
      >
        <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Agrega una nueva nota</h2>
        <input
          className='input'
          value={tarea}
          onChange={onChange}
        />
        <form onSubmit={onSubmit}>
          <button onClick={closeModal}>Cerrar</button>
          <button type="submit">Añadir</button>
        </form>
      </Modal>
        </div>
    )
}
export{Boton};