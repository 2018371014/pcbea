import React from "react";
import './item.css';

function Item(props){
  const notificarme = () => {

    if(!window.Notification){
      console.log('Soporta notificaciones')
      return
    } 
    if(Notification.permission === 'granted'){
    new Notification('Tarea marcada como completada')
    }
    else if(Notification.permission !== 'denied' || Notification.permission === 'default'){
    Notification.requestPermission((permission)=>{
      console.log(permission);
      if(permission === 'granted'){
        new Notification('Tarea marcada como completada')
      }
    })
    
      }
    }
    return(
      <div className="card">
      <div className="box">
        <div className="content">
          <h2>{props.numero}</h2>
          <p className={props.completed ? 'completado':''}>{props.text}</p>
          <button
          onClick={()=>{
            const funcion1 = props.onComplete;
            const funcion2 = props.completed ? console.log("vacio") : notificarme();
            funcion1();
            funcion2();
          }}
          >Marcar Completada</button>
          <button
          onClick={props.onEliminar}
          >
            Eliminar
            </button>
        </div>
      </div>
    </div>
    )
}
export{Item};