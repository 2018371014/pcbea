import React, { useEffect } from "react";
import './contador.css';

function Contador({total, completed}){
    useEffect(()=>{
        condicion();
    })
    const notificarme = () => {

        if(!window.Notification){
          console.log('Soporta notificaciones')
          return
        } 
        if(Notification.permission === 'granted'){
        new Notification('Has completado todas tus tareas')
        }
        else if(Notification.permission !== 'denied' || Notification.permission === 'default'){
        Notification.requestPermission((permission)=>{
          console.log(permission);
          if(permission === 'granted'){
            new Notification('Has completado todas tus tareas')
          }
        })
        
          }
        }
    const condicion = ()=>{
        if (total === 0 && completed === 0){
            console.log("Aqui no vale")
        }else{
            if (total === completed){
                notificarme();
            }else{
                console.log("Aun no bb")
            }
        }
    }
    return(
       <h2>
        Tareas completadas {completed} de {total}
       </h2>
    )
}
export{Contador};