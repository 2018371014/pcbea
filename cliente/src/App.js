import React, {useEffect} from 'react';
import axios from 'axios'
import { Titulo } from './Titulo/index';
import { Lista } from './Lista/index';
import { Item } from './Item/index';
import { Contador } from './Contador/index';
import { Buscador } from './Buscador/index';
import { Boton } from './Boton/index';
import './App.css';

function App() {

   //Estado
   const localStoragePers = localStorage.getItem('TAREAS_v1');
   let personas;
   //Estado
   const [tareas, setTareas] = React.useState([]);
  /** 
   if (!localStoragePers) {
     localStorage.setItem('TAREAS_v1', JSON.stringify([]));
     personas = [];
   } else {
     personas = JSON.parse(localStoragePers);
   }*/

   //
   useEffect(() => {
    getTareas()
  }, [])


  // funcion para agregar la tarea con get y fetch
  function getTareas(){
    fetch('http://localhost:3001/api')
      .then((response) => {
        return response.json()
      })
      .then((tareas) => {
        console.log(tareas);
        setTareas(tareas)
      })
  }
 
   //Contador

   //CONTADOR
   const [buscandoValor, setBuscandoValor] = React.useState('');
 
   //Varibles que almacena las tareas completadas
   const compledtTareas = tareas.filter((tarea) => !!tarea.completed).length;
   const totalTareas = tareas.length;
 
   let buscarTareas = [];
 
 
   //Condicional
   if (!buscandoValor.length >= 1) {
     buscarTareas = tareas;
   } else {
     buscarTareas = tareas.filter((tareas) => {
       const tareaText = tareas.text.toLowerCase();
       const buscarText = buscandoValor.toLowerCase();
       return tareaText.includes(buscarText);
     });
   }
 
   //Funcion para actualizar localStorage
   const saveTareas = (newTareas) => {
     const stringTareas = JSON.stringify(newTareas);
     localStorage.setItem('TAREAS_v1', stringTareas);
     setTareas(newTareas);

   }
 
   const addTarea = (text) => {
     const newTareas = [...tareas];
     newTareas.push({
       completed: false,
       text,
     });
     saveTareas(newTareas);
   };
 
   const completarTarea = (text) => {
     const tareaIndex = tareas.findIndex((tarea) => tarea.text === text);
     const newTareas = [...tareas];
     newTareas[tareaIndex].completed = !newTareas[tareaIndex].completed;
     saveTareas(newTareas);
   };
 
   const deleteTarea = (text) => {
     const tareaIndex = tareas.findIndex((tarea) => tarea.text === text);
     const newTareas = [...tareas];
     newTareas.splice(tareaIndex, 1);
     saveTareas(newTareas);
   };
  return (
    <React.Fragment>
    <div className="App">
      <Titulo/>
      <Contador
      total={totalTareas} 
      completed={compledtTareas}
      />
      <Buscador
       buscarValor={buscandoValor}
       setBuscarValor={setBuscandoValor}
      />
      <Lista>
        {buscarTareas.map((tarea, index)=>(
          <Item
          text={tarea.text}
          numero={'0'+ (index+1)}
          key={tarea.text}
          completed = {tarea.completed}
          onComplete={()=> completarTarea(tarea.text)}
          onEliminar={()=> deleteTarea(tarea.text)}
          />
      ))}
      </Lista>
      <Boton 
      addTarea={addTarea}
      />
    </div>
    </React.Fragment>
  );
}

export default App;
